function focusOnFilters(nav){
  nav.find('h1').click(function(){
    nav.find('li').css({'display': 'inline-block'});
    $('section').eq(0).animate({'flex-grow': '4'}, function(){
      $('#documentation').show();
      $('#projets').find('ul').show();
      nav.find('h2').show();
      goGoMaso()
    });

  })
}

function classContent(article){
  var filter = $('#categories').find('li');
  filter.click(function(){
    var theClass = $(this).attr('class');
    if (!$(this).hasClass('behind') && article.hasClass(theClass)){
      filter.removeClass('behind');
      $(this).addClass('behind');
      article.css({'opacity': '0.5'}).removeClass('behind');
      $('#projets').find('.'+theClass).css({'opacity': '1'}).addClass('behind');
    }
  })
}

function hoverProjet(article){
  article.hover(function(){
    var title = $(this).find('h1').text();
    $('#nav').find('h2').html(title);
  }, function(){
    $('#nav').find('h2').html(' ');
  })
}

function goGoMaso(){
  $('.grid').masonry({
    itemSelector: '.grid_item'
  });
}
